

function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
};

// console.log(factorial(5));

function addition(n1, n2){
	return n1 + n2;
};

function result() {
	return true;
};


const names = {
	"Brandon": {
		"name" : "Brandon Boyd",
		"age" : 35
	}, 
	"Steve": {
		"name" : "Steve Tyler",
		"age" : 56
	}
};


module.exports = {
	factorial, addition, result, names
}