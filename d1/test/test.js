const { factorial, addition, result } = require('../src/util.js');
const { expect, assert } = require('chai');

// A test case or a unit test is a single description about the desired behavior of a code that either passes or fails. 
// Test Suites are made up of collection of test cases that should be executed together. The "describe" keyword is used to group tests together.

describe('test_fun_factorials', () => {
// In mocha, the describe() function is used to group tests. It accepts a string to "describe the group of tests."
it('test_fun_factorial_5!_is_120', () => {
	// In mocha, the it() function is used to execute individual tests. It accepts a string to describe the test and a callback function to execute assertions. 
	// A callback function is a function passed into another function as an argument. 
	// A callback function is invoked after the first function is completed. 
	const product = factorial(5);
	expect(product).to.equal(120);
});
});

// There are three types of assertion styles
/*
	TDD Assertion Style: "assert"
	- TDD or test-driven development is a software development process relying on software requirements being converted to test cases before software is fully developed. In TDD, we create the test first before the actual code. 
	- Uses "programming language".
	- From "developer's point of view" and focuses on the implementation of the unit/class/feature.

	BDD Assertion Style: "expect", "should"
	- BDD or behavior-driven development is a method in which an application is documented and designed around the behavior a user expects to experience when interacting with it. 
	- Uses "natural language" to specify its test.
	- From "customer's point of view" and focuses on expected behavior of the whole system. 
	- BDD is an extension/revision of test driven development. 
	- The biggest contribution of BDD over TDD is making non-technical people (product owners/customers) a part of the software development process at all levels as writing executable scenarios in natural languages.
	- BDD is TDD with different words. User friendly words/Non-tech people friendly.
	*/

// ASSERT VS. EXPECT
/*
	Both assert and expect can be used in testing but there are slight differences.
	*/

// ASSERT
/*
	- Assert uses the assert-dot notation that node.js has. The assert module also provides several additional tests. 
	- In all cases, the assert style also allows you to include an optional message in the assert statement. 
	- Assert is not chainable. 
	*/
	describe('This is an assert example on addition', function () {
		it('should be able to test addition', function () {
			let sum = addition(2,4)
			assert.equal(sum, 6);
		});
	});

// EXPECT
// The expect module uses chainable language to construct assertions.
describe('This is an expect example on addition', function () {
	it('should be able to test addition too', function () {
		expect(addition(8,8)).to.equal(16);
		expect(addition(8,1)).to.equal(9);
		expect(addition(8,0)).to.equal(8);
		expect(addition(1,1)).to.equal(2);
	});
});

// SOME OF THE ASSERT FUNCTION
/*
	.equal
	- Asserts non-strict equality (==) of actual and expected value.
	Syntax: 
		assert.equal(actual, expected, ['message'])
		*/
		describe('this is an example of an equal function', function () {
			it('should run', function () {
				assert.equal(3, '3', '== coerces values to strings');
				assert.equal(addition(1,2), '3', '== coerces values to strings');
			});
		});

/*
	.deepEqual
	- Asserts that actual is "deeply" equal to expected.
	Syntax:
		assert.deepEqual(actual, expected, ['message'])
		*/
		describe('An example of a deepEqual', function () {
			it('should be deepEqual', function () {
		// A string cannot be cannot be equal to the number 3
		assert.deepEqual(3, 3, 'They should be deep equal');
	});
		});

/*
	.isTrue
	- Asserts that a value is true
	Syntax: 
		assert.isTrue(value, ['message'])
		*/
		describe('this is an example of the isTrue', function (){
			it('should be true', function () {
				const teaServed = true;
				assert.isTrue(teaServed, 'kahit ano');
			});
			it('should be true', function () {
				assert.isTrue(result(), 'kahit ano');
			});
		});


// EXPERIMENT ONLY
describe('Collection of assert', function () {
	it('is a collection of assert', function () {
		assert.equal(3, '3', '== coerces values to strings');
		assert.equal(3, '3', '== coerces values to strings');	
		assert.deepEqual(3, 3, 'They should be deep equal');
		assert.isTrue(result(), 'kahit ano');		
	});
});


// SOME OF THE EXPECT FUNCTIONS

/*
	.equal
	- Asserts that the target is strictly equal to the given value. 
	Syntax: 
	expect(target).to.equal(value);
*/
describe('this is an example of an expect equal', function () {
	it('should be equal', function () {
		expect(1).to.equal(1);
		expect(addition(0,1)).to.equal(1);
	});
});

/*
	.true
	- Asserts that the target is strictly (===) equal to true
	Syntax: 
	expect.(target).to.be.true;
*/
describe('this should be true', function () {
	it('should be true', function () {
		let stand = true;
		expect(stand).to.be.true;
		expect(result()).to.be.true;
	});
});